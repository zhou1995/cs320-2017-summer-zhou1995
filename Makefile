#
# A simple Makefile
#

######

all::

######

BASH=bash
WGET=wget

######

install: ; \
$(WGET) --quiet -O- \
  https://raw.githubusercontent.com/ats-lang/ats-lang.github.io/master/SCRIPT/C9-ATS2-install-cs320.sh | \
$(BASH) --verbose -s

######
#
# Adding a upstream:
#
# git remote add upstream \
#   https://bithwxi@bitbucket.org/bithwxi/cs320-2017-summer.git
#
######
#
# update:: ; \
# 	git fetch upstream
#	git checkout master
#	git merge upstream/master
#
######
#
# git-push:: ; \
# git push -u https://bitbucket.org/bithwxi/cs320-2017-summer.git
#
######

###### end of [Makefile] ######
