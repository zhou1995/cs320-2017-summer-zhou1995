(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Assign03: 30 points
//
// Out date: Tue, June 2nd, 2017
// Due date: Tue, June 9th, 2017 // by midnight
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)

typedef intrep = list0(int)

(* ****** ****** *)

(*
A non-negative integer is represented as a list of
digits. For instance
//
0 -> () // emptylst
12345 -> (5, 4, 3, 2, 1)
//
*)

(* ****** ****** *)
//
// HX: 10 points
//    
extern
fun
intrep_add
  (x: intrep, y: intrep): intrep
//
(* ****** ****** *)
//
// HX: 10 points
//    
(*
//
// Please use [intrep_add] to solve
// the following problem:
//
// https://projecteuler.net/problem=16
//
*)
extern
fun
pow2digitsum(n: int): int
//
(* ****** ****** *)
//
// HX: 10 points
//    
extern
fun
intrep_mul
  (x: intrep, y: intrep): intrep
//
(* ****** ****** *)

(* end of [assign.dats] *)
