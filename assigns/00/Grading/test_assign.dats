(* ****** ****** *)
//
#define MAIN_NONE
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
#staload "./../assign.dats"
//
#staload
Solution=
"./../Solution/assign_sol.dats"
(*
#staload
MySolution=
"./../MySolution/assign_sol.dats"
*)
//
(* ****** ****** *)

fun
test_fib_trec
(
) : bool = res where
{
//
val res = true
//
val res = res &&
(
fib_trec(10) = $Solution.fib_trec(10)
)
//
val res = res &&
(
fib_trec(20) = $Solution.fib_trec(20)
)
//
} (* end of [test_fib] *)

(* ****** ****** *)

fun
test_isTriangle
(
) : bool = res where
{
//
val res = true
//
val res = res &&
(
isTriangle(3, 4, 5)
=
$Solution.isTriangle(3, 4, 5)
)
//
val res = res &&
(
isTriangle(1, 5, 9)
=
$Solution.isTriangle(1, 5, 9)
)
//
} (* end of [test_fib] *)

(* ****** ****** *)
//
implement
main0((*void*)) =
{
//
val () =
if
test_fib_trec()
then println! ("Fib_trec: 10/10")
else println! ("Fib_trec: failed")
//
val () =
if
test_isTriangle()
then println! ("isTriangle: 10/10")
else println! ("isTriangle: failed")
//
} (* end of [main0] *)
//
(* ****** ****** *)

(* end of [test_assign.dats] *)
