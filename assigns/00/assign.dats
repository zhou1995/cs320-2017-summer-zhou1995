(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Out: Tuesday, May 23rd, 2017
// Due: Thursday, May 25th, 2017 // by midnight
//
(* ****** ****** *)
//
// #define
// ATS_PACKNAME "ASSIGN00"
//
(* ****** ****** *)

(*
Exercise 0-1 (10 points)

Please fork a private repository at bitbucket.org of the name
CS320-2017-Spring-userid based on the master repository
CS320-2017-Spring for this class, where the userid part should be
substituted with your own user id for your BU email account. Please
make sure that you are be able to receive messages sent to the email
address userid@bu.edu. Please also note that userid is not your BU
student number (starting with U followed by 8 digits).

After creating the repository of the specified name, please share it
with me (bithwxi) and the grader.
*)

(* ****** ****** *)

(*
Exercise 0-2 (10 points)

Please create a private workspace at c9.io (Cloud-9) of the name
CS320-2017-Spring-userid, where the userid part should be substituted
with your own user id for your BU email account.

After creating the workspace of the specified name, please share it
with me (c9hwxi) and the grader.
*)

(* ****** ****** *)
//
(*
Exercise 0-3-1 (5 points)

Please implement isTriangle that takes three integers and
returns true or false depending wether the 3 given integers
can form the 3 sides of a triangle. The interface for isTriangle
is given as follows:
*)
//
extern
fun
isTriangle(x: int, y: int, z: int): bool
//
(* ****** ****** *)
//
(*
Exercise 0-3-2 (5 points)

The following function fib computes Fibonacci numbers:

fun
fib(n: int): int =
  if n >= 2 then fib(n-1) + fib(n-2) else n
  
Clearly, fib is recursive but not tail-recursive: Neither of the two
recursive calls in the body of fib is a tail-call. Please give a
tail-recursive implementation of fib_trec that also computes Fibonacci
numbers:
*)
//
extern
fun
fib_trec(n: int): int // tail-recursive version of fib
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () =
assertloc(isTriangle(3,4,5))
val () =
assertloc(isTriangle(5,4,3))
val () =
assertloc(~isTriangle(1,2,3))
val () =
assertloc(~isTriangle(3,2,1))
//
val () =
println!
("Testing of [isTriangle] is done successfully!")
//
val fib =
fix f(n: int): int =>
  if n >= 2 then f(n-1)+f(n-2) else n
//
val () = assertloc(fib(5) = fib_trec(5))
val () = assertloc(fib(10) = fib_trec(10))
val () = assertloc(fib(20) = fib_trec(20))
//
val () = println! ("fib_trec(5) = ", fib_trec(5))
val () = println! ("fib_trec(10) = ", fib_trec(10))
val () = println! ("fib_trec(20) = ", fib_trec(20))
//
val () =
println! ("Testing of [fib_trec] is done successfully!")
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign.dats] *)
