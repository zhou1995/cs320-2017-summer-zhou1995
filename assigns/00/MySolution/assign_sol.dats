(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#staload "./../assign.dats"

(* ****** ****** *)
(*
//
// HX: This is a dummy
//
implement
isTriangle(x, y, z) = true
//
*)
(* ****** ****** *)
//
(*
//
// HX: This is a dummy
//
implement fib_trec(n) = 0
//
*)
//
(* ****** ****** *)
//
// HX-2017-01-19:
// Please do not modify the following code
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () =
assertloc(isTriangle(3,4,5))
val () =
assertloc(isTriangle(5,4,3))
val () =
assertloc(~isTriangle(1,2,3))
val () =
assertloc(~isTriangle(3,2,1))
//
val () =
println!
("Testing of [isTriangle] is done successfully!")
//
val fib =
fix f(n: int): int =>
  if n >= 2 then f(n-1)+f(n-2) else n
//
val () = assertloc(fib(5) = fib_trec(5))
val () = assertloc(fib(10) = fib_trec(10))
val () = assertloc(fib(20) = fib_trec(20))
//
val () = println! ("fib_trec(5) = ", fib_trec(5))
val () = println! ("fib_trec(10) = ", fib_trec(10))
val () = println! ("fib_trec(20) = ", fib_trec(20))
//
val () =
println! ("Testing of [fib_trec] is done successfully!")
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
