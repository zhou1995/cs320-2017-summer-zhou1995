(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#include "./../assign.dats"

(* ****** ****** *)

implement mylist0_last(xs)= let
val len = length(xs)
in (
case- xs of
(*| list0_nil() => ...*)
| list0_cons(x,xs1) =>
if (len = 1) then x
else mylist0_last(xs1)
)
end

implement mylist0_length(xs) = let
fun lengthhelper(xs: list0(int), len: int): int =
case xs of
| list0_nil() => len
| list0_cons(x1, xs1) => lengthhelper(xs1,len+1)
in
  lengthhelper(xs,0)
end

implement mylist0_cross(xs,ys)= 
(
 case+ xs of
 | list0_nil() => list0_nil
 | list0_cons(x0, xs1) => list0_append(list0_map<int>< $tup(int,int) >(ys, lam(y) => $tup(x0,y)), mylist0_cross(xs1,ys))
)

(*
implement mylist0_choose(xs,n)=((0,0))
*)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val
xs0 =
g0ofg1($list{int}(1,2,3,4,5))
val
ys0 =
g0ofg1($list{int}(1,2,3,4,5))
//
val () =
assertloc(mylist0_last(xs0) = 5)
//
#define N 1000000
val () =
assertloc
(
mylist0_length(list0_make_intrange(0, N)) = N
) (* end of [val] *)
//
(*
val xss = mylist0_choose(xs0, 2)
val ((*void*)) =
(
  xss.foreach()(lam(xs) => println!(xs))
)
*)
//
val xys = mylist0_cross(xs0, ys0)
val ((*void*)) =
(
  xys.foreach()(lam(xy) => println!("(", xy.0, ", ", xy.1, ")"))
)
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
