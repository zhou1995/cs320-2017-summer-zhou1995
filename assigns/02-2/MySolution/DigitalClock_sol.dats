(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)

(*
** Drawing a digital clock: 20 points
*)

(* ****** ****** *)

#include "./../DigitalClock.dats"

(* ****** ****** *)
//
// HX-2017-06-02:
//
// Please replace the following
// dummy implementations with resonable
// ones for drawing digits to show time
//
(* ****** ****** *)

implement
draw_1(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_2(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_3(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_4(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_5(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_6(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_7(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_8(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

implement
draw_9(X, Y, W, H) = draw_0(X, Y, W, H)

(* ****** ****** *)

(* end of [DigitalClock_sol.dats] *)
