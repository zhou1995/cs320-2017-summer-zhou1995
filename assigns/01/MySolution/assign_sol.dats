(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "MYSOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#staload "./../assign.dats"

(* ****** ****** *)
//
(*
//
// HX: This is a dummy
//
implement try_fact() = 
let  fun do_sth(i: int): int = 
     if fact(i) = 0 then i else fact(i-1)
in
   fact(100)
end
//
*)
//
(* ****** ****** *)
//
(*
//
// HX: This is a dummy
//
implement sumup_3_5(n0) = 
let sumhelper(i:int, total: int) int =
    if(i>n0)
    then total 
    else (i%3 = 0 or i%5 = 0 and i%15 != 0)
    then total = total + i + sumhelper(i+1,total)
in
    sumhelper(0,0)
end
//
*)
//
(* ****** ****** *)

// 
implement money2pence(m0) = // 5 points
 m0.0*240+m0.1*12+m0.2*1
// pence2money(n0) = // 5 points
if (n0<12)
then $tup(0,0,n0/1)
if (n0>=12 and n0<240)
then $tup(0,(n0-n0%12)/12,n0%12)
else $tup((n0-n0%240)/240,(n0-(n0-n0%240)%12)/12,(n0-n0%240)%12)
(* ****** ****** *)

// 
implement test_colinearity(p1,p2,p3) =// 10 points
if ((p1.1 - p2.1)/(p1.0 - p2.0) = (p1.1 - p3.1)/(p1.0 - p3.0))
  then true
else false
(* ****** ****** *)

//triangle_median_inner(A) = // 10 points
implement triangle_median_outer(A) =
a:point=((A.0.0+A.1.0)/2.0,(A.0.1+A.1.1)/2.0,(A.0.2+A.1.2)/2.0)
b:point=((A.0.0+A.2.0)/2.0,(A.0.1+A.2.1)/2.0,(A.0.2+A.2.2)/2.0)
c:point=((A.1.0+A.2.0)/2.0,(A.1.1+A.2.1)/2.0,(A.1.2+A.2.2)/2.0)
$tup(a,b,c)

/triangle_median_outer(A) // 10 points
implement triangle_median_outer(A) =
let fun outerhelper(a:point, b:point, c:point):point =
$tup(a.0+b.0-c.0, a.1+b.1-c.1,a.2+b.2-c.2)
in
$tup(outerhelper(A.0,A.1,A.2),outerhelper(A.2,A.0,A.1),outerhelper(A.1,A.2,A.0)
end
(* ****** ****** *)
//
// HX-2017-01-19:
// Please do not modify the following code
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () =
println!
  ("try_fact() = ", try_fact())
//
val () =
println!
  ("sumup_3_5(1000) = ", sumup_3_5(1000))
//
val () = assertloc(money2pence(pence2money(1000)) = 1000)
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
