(* ****** ****** *)
//
// HX:
//
// How to test:
// ./assign_sol
// How to compile:
// myatscc assign_sol.dats
//
(* ****** ****** *)
(*
\#\#myatsccdef=\
curl --data-urlencode mycode@$1 \
http://bucs320-tutoriats.rhcloud.com/\
SERVICE/assets/patsopt_tcats_eval_code_0_.php | \
php -R 'if (\$argn != \"\") echo(json_decode(urldecode(\$argn))[1].\"\\n\");'
*)
(* ****** ****** *)
//
#define
ATS_DYNLOADFLAG 0
//
(* ****** ****** *)
//
#define
ATS_PACKNAME "SOLUTION"
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#staload "./../assign.dats"

(* ****** ****** *)
//
// HX: This is a dummy
//
implement
try_fact() =
(
fix
loop
(i: int, res: int): int =>
if res = 0
  then i else loop(i+1, (i+1)*res)
// end of [loop]
)(0, 1)
//
//
(* ****** ****** *)
//
(*
implement
sumup_3_5(n0) =
(
//
fix
loop
(
i: int, res: int
): int =<cloref1>
if
(i < n0)
then
(
ifcase
| (i%15 = 0) => loop(i+1, res)
| (i % 3 = 0) => loop(i+1, res + i)
| (i % 5 = 0) => loop(i+1, res + i)
| _(* else *) => loop(i+1, res)
) (* end of [then] *)
else res // end of [else]
//
)( 1, 0 ) (* end of [sumup_3_5] *)
*)
//
(* ****** ****** *)

implement
sumup_3_5(n0) = let
//
fun
f(k: int): int = g(k, 0, 0)
and
g(k: int, i: int, res: int): int =
  if i < n0 then g(k, i+k, res+i) else res
//
in
  f(3) + f(5) - 2*f(15)
end // end of [sumup_3_5]

(* ****** ****** *)
//
(*
// money2pence // 5 points
// pence2money // 5 points
*)
//
implement
money2pence(m) =
240 * m.0 + 12 * m.1 + m.2
//
implement
pence2money
  (x) = let
  val p = x / 240
  val x = x % 240
  val s = x /  12
  val x = x %  12 in $tup(p, s, x)
end // end of [pence2money]
//
(* ****** ****** *)
//
fun
fprint_point
(out: FILEref, P: point): void =
fprint!(out, "(", P.0, ", ", P.1, ", ", P.2, ")")
//
fun
fprint_triangle
(out: FILEref, T: triangle): void = let
//
overload fprint with fprint_point
//
in
  fprint!(out, "(", T.0, ", ", T.1, ", ", T.2, ")")
end // end of [fprint_triangle]
//
(* ****** ****** *)
(*
// test_colinearity // 10 points
*)
//
#define EPSILON 1E-6
//
fun
is_epsilon
  (x: double): bool = (abs(x) < EPSILON)
//
implement
test_colinearity
  (p1, p2, p3) = let
//
val x1 = p1.0 and y1 = p1.1 and z1 = p1.2
//
val x2 = p2.0 and y2 = p2.1 and z2 = p2.2
//
val x3 = p3.0 and y3 = p3.1 and z3 = p3.2
//
in
is_epsilon
  (x1*y2*z3 + y1*z2*x3 + z1*x2*y3 - z1*y2*x3 - y1*x2*z3 - x1*z2*y3)
end // end of [test_colinearity]

(* ****** ****** *)

typedef
vector =
$tup(double, double, double)

(* ****** ****** *)
//
fun
point_diff
(P1: point, P2: point): vector =
  $tup(P1.0-P2.0, P1.1-P2.1, P1.2-P2.2)
//
fun
point_shift
(P1: point, V2: vector): point =
  $tup(P1.0+V2.0, P1.1+V2.1, P1.2+V2.2)
//
fun
vector_scale
(K: double, V0: vector): vector = $tup(K*V0.0, K*V0.1, K*V0.2)
//
(* ****** ****** *)
(*
// triangle_median_inner // 10 points
// triangle_median_outer // 10 points
*)
(* ****** ****** *)

implement
triangle_median_inner
  (ABC) = let
//
val A = ABC.0
val B = ABC.1
val C = ABC.2
//
val AB = point_diff(B, A)
val BC = point_diff(C, B)
val CA = point_diff(A, C)
//
in
//
$tup
(
point_shift(A, vector_scale(0.5, AB))
,
point_shift(B, vector_scale(0.5, BC))
,
point_shift(C, vector_scale(0.5, CA))
)
//
end // end of [triangle_median_inner]

implement
triangle_median_outer
  (ABC) = let
//
val A = ABC.0
val B = ABC.1
val C = ABC.2
//
val AB = point_diff(B, A)
val BC = point_diff(C, B)
val CA = point_diff(A, C)
//
in
//
$tup
(
point_shift(A, BC),point_shift(B, CA), point_shift(C, AB)
) (* $tup *)
//
end // end of [triangle_median_outer]

(* ****** ****** *)
//
// HX-2017-01-19:
// Please do not modify the following code
//
(* ****** ****** *)

#ifdef
MAIN_NONE
#then
#else

implement
main0() = () where
{
//
val () =
println!
  ("try_fact() = ", try_fact())
//
val () =
println!
  ("sumup_3_5(1000) = ", sumup_3_5(1000))
//
val N = 1000
val N3 = (N-1)/3
val N5 = (N-1)/5
val N15 = (N-1)/15
val () =
assertloc
(
sumup_3_5(1000) =
3*(N3*(N3+1))/2+5*(N5*(N5+1))/2-2*15*(N15*(N15+1))/2
) (* assertloc *)
//
val () = assertloc(money2pence(pence2money(1000)) = 1000)
//
val P1 = $tup(1.23, 2.34, 3.45)
val P2 = $tup(12.34, 34.56, 56.78)
val P3 = point_shift(P1, vector_scale(10.0, point_diff(P2, P1)))
//
val () = println!("test_colinearity = ", test_colinearity(P1, P2, P3))
//
val A = $tup(1.23, 2.34, 3.45)
val B = $tup(1.23+3, 2.34+4, 3.45+5)
val C = $tup(1.23+7, 2.34+8, 3.45+9)
//
val ABC = $tup(A, B, C)
val ABC2 = triangle_median_inner(triangle_median_outer(ABC))
//
val () =
  fprintln!(stdout_ref, "ABC:")
val () =
  fprint_triangle(stdout_ref, ABC)
val () = fprint_newline(stdout_ref)
//
val () =
  fprintln!(stdout_ref, "ABC2:")
val () =
  fprint_triangle(stdout_ref, ABC2)
val () = fprint_newline(stdout_ref)
//
} (* end of [main0] *)

#endif // end of #ifdef(MAIN_NONE)

(* ****** ****** *)

(* end of [assign_sol.dats] *)
