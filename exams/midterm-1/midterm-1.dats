(* ****** ****** *)
//
// Title:
// Concepts of
// Programming Languages
// Course: CAS CS 320
//
// Semester: Summer I, 2017
//
// Classroom: KCB 102
// Class Time: MTWR 2:00-4:00
//
// Instructor: Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Midterm 1
//
(* ****** ****** *)
//
// Out: 12:00 on the 8th of June, 2017
// Due: 11:59pm on the 9th of June, 2017
//
(* ****** ****** *)
//
// ATTENTION:
// Absolutely no collaboration in any form is allowed!!!
//
(* ****** ****** *)

(*
//
Group1:
//
Please choose six:
//
A_draw: 10 points
//
sublist_test: 10 points
//
mylist_split: 10 points
mylist_pairing: 10 points
mylist_prefixes: 10 points
//
mytree_filter: 10 points
mytree_maxminHT: 10 points
mytree_is_braun: 10 points
//
mylist2brauntree: 10 points
//
Note that You can only choose six.
//
*)

(* ****** ****** *)

(*
//
Group2:
//
// Please choose one:
//
mylist_submaxord: 20 points
mylist_perm_succ: 20 points
//
// If you solve both correctly,
// you will get 20 bonus points
//
*)

(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
#include
"share/HATS\
/atspre_staload_libats_ML.hats"
//
(* ****** ****** *)
//
datatype
tree0(a:t@ype) =
  | tree0_nil of ()
  | tree0_cons of (tree0(a), a, tree0(a))
//
(* ****** ****** *)
//
extern
fun
{a,b:t@ype}
mytree_fold
(
  xs: tree0(a), fopr: (b, a, b) -<cloref1> b, sink: b
) : b // mytree_fold
//
implement
{a,b}
mytree_fold
  (xs, fopr, sink) = let
//
fun aux(xs: tree0(a)): b =
  case+ xs of
  | tree0_nil() => sink
  | tree0_cons(xs_l, x0, xs_r) => fopr(aux(xs_l), x0, aux(xs_r))
//
in
  aux(xs)
end // end of [mytree_fold]
//
(* ****** ****** *)

(*
//
// HX: 10 points
// The function A_draw is called to draw the letter 'A':

A_draw(1):

  *
 ***
*   *

A_draw(2):

    *
   * *
  *****
 *     * 
*       *

You may assume that A_draw can only be called on positive
integers.
*)

extern
fun
A_draw(n: int): void

(* ****** ****** *)
//
(*
**
** HX: 10 points
**
*)
//
// A list is a sublist of another list if the former can be obtained from
// removing some elements in the latter. For instance, (1,3,5) is a sublist
// of (1,2,3,4,5) but (1,1) is not a sublist of (1,2,3).
//
// Given two lists [xs] and [ys], the function sublist_test(xs, ys) returns
// true if and only if [ys] is a sublist of [xs]:
//
extern
fun sublist_test (xs: list0(int), ys: list0(int)): bool
//
(* ****** ****** *)
//
// HX: 10 points
//
// Given a binary tree and a predicate,
// [mytree_filter] returns a list consisting of
// all the elements in the tree that satisfiyes
// the predicate.
//
extern
fun{a:t@ype}
mytree_filter(tree0(a), p: (a) -> bool): list0(a)
//
(* ****** ****** *)
  
(*
//
HX: 10 points
[mylist_prefixes] returns a list
consisting of all the prefixes of a given list.
For instance, given (0, 1, 2), mylist_prefixes
returns the following list of lists:
//
        ((), (0), (0, 1), (0, 1, 2))
//
*)
extern
fun{a:t@ype}
mylist_prefixes(xs: list0 (a)): list0(list0(a))
//
(* ****** ****** *)
//
// HX: 10 points
//
// Given a list xs = (x_1, x_2, ..., x_{n-1}, x_n),
// [mylist_pairing] returns ((x_1, x_n), (x_2, x_{n-1}), ...)
//
// Say xs = (1, 2, 3, 4, 5, 6). Then the result equals
// ((1, 6), (2, 5), (3, 4).
//
// You may assume that [xs] is always a list containing an even
// number of elements.
//
extern
fun{a:t@ype}
mylist_pairing(xs: list0(a)): list0($tup(a, a))
//
(* ****** ****** *)
//
// HX: 10 points
//
(*
** [mylist0_split] splits a list alternately.
** For instance, if the given list is (1, 2, 3, 4, 5)
** then it is splitted into (1,3,5) and (2,4)
*)
//
extern
fun{
a:t@ype
} mylist0_split(xs: list0(a)): $tup(list0(a), list0(a))
//
(* ****** ****** *)

(*
//
// 10 points
//
The height of a tree is the length of the longest path from
the root of the tree to a leaf note. Let us call this "maxHeight".
There is a related concept "minHeight", which refers the length of
the *shortest* path from the root to a leaf node.
//
Please implement a function for computing maxHeight and minHeight
of a given binary tree by making exactly *one* traversal of the
given tree. Note that a solution like the following one makes two
traversals:
//
implement
{a}
mytree_maxminHT
  (t0) = (maxHT(t0), minHT(t0))
//
maxHT does one traversal and minHT does another traversal.
//
In particular, a solution receiving full credit cannot make use of
pattern matching explicitly on tree0-values.
//
Hint:
When calling mytree_fold (given above), the initial value should be
a pair of integers $tup(0, 0)
//
*)
extern
fun{a:t@ype}
mytree_maxminHT(t0: tree0 (a)): $tup(int, int)

(* ****** ****** *)
//
(*
//
** HX: 10 points
** A Braun tree is defined as follows:
**
** 1. tree0_nil is a Braun tree
** 2. tree0_cons(t_l, _, t_r) is a Braun tree
** if both t_l and t_r are Braun trees and also
** size(t_r) <= size(t_l) <= size(t_r) + 1
**
** Please implement [mytree_is_braun] which tests
** whether a given tree is a Braun tree or not.
//
*)
//
extern
fun{a:t@ype}
mytree_is_braun(xs: tree0(a)): bool
//
(* ****** ****** *)
//
// HX: 10 points
//
(*
mylist2brauntree turns a given list xs into a Braun tree
such that flatten(mylist2brauntree(xs)) = xs
*)
extern
fun{a:t@ype}
mylist2brauntree(xs: list0(a)): tree0(a)
//
(* ****** ****** *)
//
// HX: 20 points
//
// Given a list xs of integers, the function
// [mylist_submaxord] returns the longest leftmost
// subsequence of xs that is ordered.
//
// If xs = (1, 3, 2, 4), then the result is
// (1, 3, 4) (not (1, 2, 4)).
//
// if xs = (0, 1, 3, 2, 2, 4), then the result is
// (0, 1, 2, 2, 4).
//
// If xs = (~1, ~1, 4, 1, 2, 3, 8, 9, 5, 6, 7), then the
// result is (~1, ~1, 1, 2, 3, 5, 6, 7)
//
extern
fun
mylist_submaxord(xs: list0(int)): list0(int)
//
(* ****** ****** *)

(*
//
// Q10: 20 points
//
// The permutations of 0,1,2 can be ordered
// according to the lexicographic ordering as follows:
//
// (0,1,2) < (0,2,1) < (1,0,2) < (1,2,0) < (2,0,1) < (2,1,0)
//
// This ordering can be readily generalized to the permuations
// of n numbers, which n is positive. Given a permutation xs of
// the first n natural numbers, perm_succ(xs) returns the next
// permutation following xs if it exists.
//
*)
//
extern
fun mylist_perm_succ(xs: list0(int)): option0(list0(int))
//
(* ****** ****** *)

(* end of [midterm-1.dats] *)
