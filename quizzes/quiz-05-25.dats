(* ****** ****** *)
//
// quiz-05-25-1:
//
fun f91(x: int): int =
if x > 100
  then x - 10 else f91(f91(x+11))
//
(*
(A) There is no tail-recursive call in f91
(A) There is (1) tail-recursive call in f91
(A) There are (2) tail-recursive calls in f91
(D) None of the above
*)
//
(* ****** ****** *)
//
// quiz-05-25-2:
//
fun foo(x: int): int =
  if x >= 0 then foo(x+1) else (x)
//
(*
(A): foo(0) runs forever
(B): foo(0) crashes
(C): foo(0) returns some integer value
(D): None of the above
*)
//
(* ****** ****** *)

(* end of [quiz-05-25.dats] *)