######
#
# Schedule for
# CS320, Summer I, 2017
#
######

Tue, May 22:

Introduction.
Setup for the class: Bitbucket; Cloud9
Implementing, typechecking, compiling and
testing the HelloWorld problem in ATS

Wed, May 24:
(Only one-hour class)

If-then-else expressions.
Recursion. Tail-recursion.

Thu, May 25:

names vs. variables.
Let-in-end construct for introducing local names.
Tuples. Inner functions.

######

Mon, May 29: Holidy

Tue, May 30
Higher-order functions.
First Datatype example: mylist

Wed, May 31
List-processing of combinator style

Thu, Jun 01
More list-processing of combinator style

Fri, Jun 02
Two hour session for questions

######

Mon, Jun 05
Only one-hour lecture:
List-based Matmul of combinator style

Tue, Jun 06
Introduction to streams.
Some stream combinators: map and filter

Wed, Jun 07
Sieve of Eratosthenes (for one-half-hour)

Thu, Jun 08: Midterm-1

######

Mon, Jun 12

Tue, Jun 13
Wed, Jun 14

Thu, Jun 15

######

Mon, Jun 19

Tue, Jun 20
Wed, Jun 21

Thu, Jun 22: Midterm-2

######

Mon, Jun 26

Tue, Jun 27
Wed, Jun 28

Thu, Jun 29: Final Exam

###### end of [schedule.txt] ######
